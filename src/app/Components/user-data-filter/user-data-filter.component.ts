import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FiltersModel } from 'src/app/Shared/Filters.model';

@Component({
  selector: 'app-user-data-filter',
  templateUrl: './user-data-filter.component.html',
  styleUrls: ['./user-data-filter.component.css']
})
export class UserDataFilterComponent implements OnInit, OnDestroy {

  form: FormGroup;
  @Output() onSettingsChange: EventEmitter<{}> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    const filters = JSON.parse(localStorage.getItem('filters')) || new FiltersModel();
    this.form = new FormGroup({
      city: new FormControl(filters.city),
      gender: new FormControl(filters.gender),
      street: new FormControl(filters.street),
      email: new FormControl(filters.email),
      phone: new FormControl(filters.phone)
    });

    this.onSettingsChange.emit(this.form.value);
    this.form.valueChanges.subscribe(res => {
      localStorage.setItem('filters', JSON.stringify(this.form.value));
      this.onSettingsChange.emit(res);
    });
  }

  ngOnDestroy() {
  }
}
