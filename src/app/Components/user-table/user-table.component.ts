import { Component, OnInit, Input } from '@angular/core';
import { UserTableService } from 'src/app/Services/user-table.service';
import { IUser } from 'src/app/Shared/IUser';
import { FiltersModel } from 'src/app/Shared/Filters.model'; 

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {

  @Input() filters: FiltersModel;
  public userTable: IUser[];

  constructor(
    private userTableService: UserTableService
  ) { }

  ngOnInit() {
    this.userTableService.getUserTable()
    .then((res: IUser[]) => this.userTable = res);
  }
}
