import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.css']
})
export class IndexPageComponent implements OnInit {

  filters: {};

  constructor() { }

  ngOnInit() {
  }

  setFilters(event: any) {
    this.filters = event;
  }

}
