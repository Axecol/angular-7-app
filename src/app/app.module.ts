import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import {
  DxButtonModule,
  DxDataGridModule
} from 'devextreme-angular';

import { RouterModule } from '@angular/router';
import { routes } from './app.routing';

import { AppComponent } from './app.component';
import { IndexPageComponent } from './Pages/index-page/index-page.component';
import { UserTableComponent } from './Components/user-table/user-table.component';
import { UserDataFilterComponent } from './Components/user-data-filter/user-data-filter.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexPageComponent,
    UserTableComponent,
    UserDataFilterComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    ReactiveFormsModule,

    DxButtonModule,
    DxDataGridModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
