import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HTTPRequestService {

  constructor(
    private http: HttpClient
  ) { }

  get(url: string): Observable<object> {
    console.log(1);
    return this.http.get(url)
      .pipe(
        map(res => res['results'])
      );
  }
}
