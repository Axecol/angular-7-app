import { Injectable } from '@angular/core';
import { HTTPRequestService } from './HTTP-request.service';
import { map, mergeMap } from 'rxjs/operators';
import { IUser } from '../Shared/IUser';

@Injectable({
  providedIn: 'root'
})
export class UserTableService {

  constructor(
    private httpClient: HTTPRequestService
  ) { }

  getUserTable() {
    return this.httpClient.get('https://randomuser.me/api?results=100')
      .pipe(
        map((res: []) => {
          res.forEach((elem: {}) => {
            elem['name'] = `${ elem['name'].first} ${ elem['name'].last}`;
          });

          return res;
        })
      )
      .toPromise();
  }
}
